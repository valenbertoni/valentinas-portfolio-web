import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { ENDPOINTS } from 'src/app/endpoints/endPoints';
import { Iabout } from '../models/IAbout';

@Injectable({
  providedIn: 'root'
})
export class AboutService {
  private aboutEndPoint: string = ENDPOINTS.about;

  constructor(private http: HttpClient) {/*Empty*/ }

  public getAbout(): Observable<any> {
    return this.http.get(this.aboutEndPoint).pipe(
      map((response: Iabout[]) => {
        if (!response) {
          throw new Error('value expected');
        } else {
          return response;
        }
      }),
      catchError ((err) => {
        throw new Error (err.message);
      })
    );
  }
}

import { Component, OnInit } from '@angular/core';
import { Iabout } from '../models/IAbout';
import { AboutService } from '../services/about.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  public dataAbout: Iabout[];

  constructor(private aboutService: AboutService) { }

  ngOnInit(): void {
    this.getAboutData();
  }

  public getAboutData(): void {
    this.aboutService.getAbout().subscribe(
      (data: Iabout[]) => {
        this.dataAbout = data;
        console.log(this.dataAbout);
      },
      (err) => {
        console.error(err.message);
      }
    );
  }
}

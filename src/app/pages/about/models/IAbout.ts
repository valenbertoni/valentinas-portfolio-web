export interface Iabout {
    aboutTitle: string;
    aboutDescription: string;
    aboutDescription2: string;
    expertiseTitle: string;
    expertise: string[];
    skillsTitle: string;
    skills: string[];
    workExperienceTitle: string;
    workExperienceBlock: experienceBlock;
}

export interface experienceBlock {
    year: number;
    position: string;
    company: string;
}
import { Component, OnInit } from '@angular/core';

import {
  Iwork,
  IworkContact,
  IworkHero,
  IworkProjects,
} from './../models/IWorks';
import { WorkService } from '../services/work.service';

@Component({
  selector: 'app-work',
  templateUrl: './work.component.html',
  styleUrls: ['./work.component.scss'],
})
export class WorkComponent implements OnInit {
  public dataWorkHero: IworkHero;
  public dataWorkProjects: IworkProjects;
  public dataWorkContact: IworkContact;

  constructor(private workService: WorkService) {}

  ngOnInit(): void {
    this.getWorkData();
  }

  public getWorkData(): void {
    this.workService.getWork().subscribe(
      (data: Iwork[]) => {
        this.mapWork(data);
        console.log(data);
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

  public mapWork(data: Iwork[]): void {
    this.dataWorkHero = {
      heroTitle: data[0].heroTitle,
      heroDescription: data[0].heroDescription,
      heroImage: data[0].heroImage,
      projects: data[0].projects,
    };
    // this.dataWorkProjects = {
    //   projectImg: data[].projectImg,
    //   projectCategory: data[].projectCategory,
    //   projectTitle: data[].projectTitle,
    //   projectDescription: data[].projectDescription,
    // }
    this.dataWorkContact = {
      contactTitle: data[0].contactTitle,
      contactDescription: data[0].contactDescription,
    };
    console.log(this.dataWorkProjects);
  }
}

import { Component, Input, OnInit } from '@angular/core';
import {  IworkProjects } from '../../models/IWorks';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
@Input() public dataWorkProjects: IworkProjects;
  constructor() { }

  ngOnInit(): void {
  }

}

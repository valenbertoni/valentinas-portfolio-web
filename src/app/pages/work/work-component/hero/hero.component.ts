import { Component, Input, OnInit } from '@angular/core';
import { IworkHero } from '../../models/IWorks';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements OnInit {
@Input() public dataWorkHero: IworkHero;
  constructor() { }

  ngOnInit(): void {
  }

}

import { IworkContact } from './../../models/IWorks';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
@Input() public dataWorkContact: IworkContact;
  constructor() { }

  ngOnInit(): void {
  }

}

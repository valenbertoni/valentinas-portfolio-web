import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkRoutingModule } from './work-routing.module';
import { WorkComponent } from './work-component/work.component';
import { HeroComponent } from './work-component/hero/hero.component';
import { ContactComponent } from './work-component/contact/contact.component';
import { ProjectsComponent } from './work-component/projects/projects.component';


@NgModule({
  declarations: [WorkComponent, HeroComponent, ContactComponent, ProjectsComponent ],
  imports: [
    CommonModule,
    WorkRoutingModule
  ]
})
export class WorkModule { }

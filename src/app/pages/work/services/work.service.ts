import { Iwork } from './../models/IWorks';
import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { ENDPOINTS } from 'src/app/endpoints/endPoints';

@Injectable({
  providedIn: 'root',
})
export class WorkService {
  private workEndPoint: string = ENDPOINTS.work;

  constructor(private http: HttpClient) {
    /*Empty*/
  }

  public getWork(): Observable<any> {
    return this.http.get(this.workEndPoint).pipe(
      map((response: Iwork[]) => {
        if (!response) {
          throw new Error('value expected');
        } else {
          return response;
        }
      }),
      catchError ((err) => {
        throw new Error (err.message);
      })
    );
  }
}

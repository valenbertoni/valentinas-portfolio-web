export interface Iwork {
  heroTitle: string;
  heroDescription: string;
  heroImage: Img;
  projects: IworkProjects;
  contactTitle: string;
  contactDescription: string;
}
export interface IworkHero {
  heroTitle: string;
  heroDescription: string;
  heroImage: Img;
  projects: IworkProjects;
}

export interface IworkContact {
  contactTitle: string;
  contactDescription: string;
}

 export interface IworkProjects {
     projectImg: Img;
     projectCategory: string;
     projectTitle: string;
     projectDescription: string;
}
export interface Img {
  url: string;
  alt: string;
}

import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { ENDPOINTS } from 'src/app/endpoints/endPoints';
import { Icontact, IcontactForm } from '../models/IContact';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private contactEndPoint: string = ENDPOINTS.contact;
  private dataUserEndPoint: string = ENDPOINTS.dataUser;

  constructor(private http: HttpClient) {/*Empty*/}

  public getContact(): Observable<any> {
    return this.http.get(this.contactEndPoint).pipe(
      map((response: Icontact) => {
        if (!response) {
          throw new Error('value expected');
        } else {
          return response;
        }
      }),
      catchError ((err) => {
        throw new Error (err.message);
      })
    );
  }


  public postUser(user: IcontactForm): Observable<any> {
    return this.http.post(this.dataUserEndPoint, user).pipe(
      map((response: IcontactForm) => {
        if (!response) {
          throw new Error('value expected');
        } else {
          return response;
        }
      }),
      catchError ((err) => {
        throw new Error (err.message);
      })
    );
  }
    
}

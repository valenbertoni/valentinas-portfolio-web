export interface Icontact {
    contactTitle: string;
}

export interface IcontactForm {
    id: number;
    name: string;
    lastname: string;
    mail: string;
    company: string;
    message: string;
}
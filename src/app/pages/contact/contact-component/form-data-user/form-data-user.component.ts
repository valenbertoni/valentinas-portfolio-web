import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IcontactForm } from '../../models/IContact';
import { ContactService } from '../../services/contact.service';

@Component({
  selector: 'app-form-data-user',
  templateUrl: './form-data-user.component.html',
  styleUrls: ['./form-data-user.component.scss'],
})
export class FormDataUserComponent implements OnInit {
  public formUser: FormGroup | null = null;
  public submitted: boolean = false;
  public userData: IcontactForm;

  constructor(
    private formBuilder: FormBuilder,
    private contactService: ContactService) { }

  ngOnInit(): void {
    this.createUser();
  }

  public createUser():void {
    this.formUser = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      lastname: ['', [Validators.required, Validators.minLength(2)]],
      mail: ['', [Validators.required, Validators.email]],
      company: [''],
      message: ['', [Validators.required]],
    });
  }

  public onSubmit(): void {
    this.submitted = true;
    if (this.formUser.valid) {
      this.postUser();
      this.formUser.reset();
      this.submitted = false;
    }
  }

  public postUser(): void {
    const user: IcontactForm = {
      id: 2,
      name: this.formUser.get('name').value,
      lastname: this.formUser.get('lastname').value,
      mail: this.formUser.get('mail').value,
      company: this.formUser.get('company').value,
      message: this.formUser.get('message').value,
    };
    this.contactService.postUser(user).subscribe(
      (data) => {
        console.log(data);
      },
      (err) => {
        console.error(err.message);
      }
    );
  }
}

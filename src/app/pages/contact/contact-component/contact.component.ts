import { Component, OnInit } from '@angular/core';
import { Icontact, IcontactForm } from '../models/IContact';
import { ContactService } from '../services/contact.service';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  public dataContact: Icontact;

  constructor(private contactService: ContactService) { }

  ngOnInit(): void {
    this.getContactData();
  }

  public getContactData(): void {
    this.contactService.getContact().subscribe(
      (data: Icontact) => {
        this.dataContact = data;
        console.log(this.dataContact);
      },
      (err) => {
        console.error(err.message);
      }
    );
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ContactRoutingModule } from './contact-routing.module';
import { ContactComponent } from './contact-component/contact.component';
import { FormDataUserComponent } from './contact-component/form-data-user/form-data-user.component';


@NgModule({
  declarations: [ContactComponent, FormDataUserComponent],
  imports: [
    CommonModule,
    ContactRoutingModule,
    ReactiveFormsModule
  ]
})
export class ContactModule { }

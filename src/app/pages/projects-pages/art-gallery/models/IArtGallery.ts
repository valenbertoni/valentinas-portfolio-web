export interface IartGallery {
    id: string;
    projectTitle: string;
    projectDescription: string;
    projectImgPrincipal: Img;
    myRolTitle: string;
    myRolDescription: string;
    viewTitle: string;
    viewImg: Img;
    viewCopy: string;
    view2Title: string;
    view2Img: Img;
    view2Copy: string;
    conclusionTitle: string;
    conclusionCopy: string;
}

export interface Img {
    url: string;
    alt: string;
  }
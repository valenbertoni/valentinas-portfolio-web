import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArtGalleryComponent } from './art-gallery-components/art-gallery.component';

const routes: Routes = [{
  path:'',
  component: ArtGalleryComponent,
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArtGalleryRoutingModule { }

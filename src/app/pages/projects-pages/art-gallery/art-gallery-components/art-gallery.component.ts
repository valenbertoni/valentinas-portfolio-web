import { Component, OnInit } from '@angular/core';
import { IartGallery } from '../models/IArtGallery';
import { ArtGalleryService } from '../services/art-gallery.service';

@Component({
  selector: 'app-art-gallery',
  templateUrl: './art-gallery.component.html',
  styleUrls: ['./art-gallery.component.scss']
})
export class ArtGalleryComponent implements OnInit {
  public dataArtGallery: IartGallery;

  constructor(private artGalleryService: ArtGalleryService) { }

  ngOnInit(): void {
    this.getArtGalleryData();
  }

  public getArtGalleryData(): void {
    this.artGalleryService.getArtGallery().subscribe(
      (data: IartGallery) => {
        this.dataArtGallery = data;
        console.log(this.dataArtGallery);
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

}
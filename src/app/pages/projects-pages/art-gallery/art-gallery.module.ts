import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArtGalleryRoutingModule } from './art-gallery-routing.module';
import { ArtGalleryComponent } from './art-gallery-components/art-gallery.component';


@NgModule({
  declarations: [ArtGalleryComponent],
  imports: [
    CommonModule,
    ArtGalleryRoutingModule
  ]
})
export class ArtGalleryModule { }

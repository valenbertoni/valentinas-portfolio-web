import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { ENDPOINTS } from 'src/app/endpoints/endPoints';
import { IartGallery } from '../models/IArtGallery';

@Injectable({
  providedIn: 'root'
})
export class ArtGalleryService {
  private artGalleryEndPoint: string = ENDPOINTS.artGallery;

  constructor(private http: HttpClient) {/*Empty*/ }

  public getArtGallery(): Observable <any> {
    return this.http.get(this.artGalleryEndPoint).pipe(
      map((response: IartGallery) => {
        if (!response) {
          throw new Error ('value expected')
        }
        else {
          return response;
        }
      }),
      catchError ((err) => {
        throw new Error (err.message);
      })
    );
  }
}

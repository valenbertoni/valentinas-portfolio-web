import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SounditiRoutingModule } from './sounditi-routing.module';
import { SounditiComponent } from './sounditi-components/sounditi.component';


@NgModule({
  declarations: [SounditiComponent],
  imports: [
    CommonModule,
    SounditiRoutingModule
  ]
})
export class SounditiModule { }

export interface Isounditi {
    id: string;
    projectTitle: string;
    projectDescription: string;
    projectImgPrincipal: Img;
    myRolTitle: string;
    myRolDescription: string;
    viewTitle: string;
    viewImg: Img;
    viewCopy: string;
    view2Img: Img;
    view2Copy: string;
    conclusionCopy: string;
}

export interface Img {
    url: string;
    alt: string;
}
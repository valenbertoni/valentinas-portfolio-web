import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SounditiComponent } from './sounditi-components/sounditi.component';

const routes: Routes = [
  {
    path: '',
    component: SounditiComponent,

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SounditiRoutingModule { }

import { TestBed } from '@angular/core/testing';

import { SounditiService } from './sounditi.service';

describe('SounditiService', () => {
  let service: SounditiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SounditiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

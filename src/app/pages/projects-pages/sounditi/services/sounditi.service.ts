import { Isounditi } from './../models/ISounditi';
import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { ENDPOINTS } from 'src/app/endpoints/endPoints';

@Injectable({
  providedIn: 'root'
})
export class SounditiService {
  private sounditiEndPoint: string = ENDPOINTS.sounditi;

  constructor(private http: HttpClient) { /*Empty*/}

  public getSounditi(): Observable<any> {
    return this.http.get(this.sounditiEndPoint).pipe(
      map((response: Isounditi) => {
        if (!response) {
          throw new Error ('value expected')
        } else {
          return response;
        }
      }),
      catchError ((err)=> {
        throw new Error (err.message);
      })
    );
  }
}

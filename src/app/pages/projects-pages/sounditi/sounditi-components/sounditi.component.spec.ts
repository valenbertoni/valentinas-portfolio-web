import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SounditiComponent } from './sounditi.component';

describe('SounditiComponent', () => {
  let component: SounditiComponent;
  let fixture: ComponentFixture<SounditiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SounditiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SounditiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { SounditiService } from './../services/sounditi.service';
import { Isounditi } from './../models/ISounditi';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sounditi',
  templateUrl: './sounditi.component.html',
  styleUrls: ['./sounditi.component.scss']
})
export class SounditiComponent implements OnInit {
  public dataSounditi: Isounditi;

  constructor(private sounditiService: SounditiService) { }

  ngOnInit(): void {
    this.getSounditiData();
  }

  public getSounditiData():void {
    this.sounditiService.getSounditi().subscribe(
      (data: Isounditi) => {
        this.dataSounditi = data;
        console.log(this.dataSounditi);
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

}

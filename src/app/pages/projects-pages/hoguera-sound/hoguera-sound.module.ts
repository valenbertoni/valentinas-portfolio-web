import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HogueraSoundRoutingModule } from './hoguera-sound-routing.module';
import { HogueraSoundComponent } from './hoguera-components/hoguera-sound.component';


@NgModule({
  declarations: [HogueraSoundComponent],
  imports: [
    CommonModule,
    HogueraSoundRoutingModule
  ]
})
export class HogueraSoundModule { }

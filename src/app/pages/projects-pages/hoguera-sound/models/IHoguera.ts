export interface Ihoguera {
    id: string;
    projectTitle: string;
    projectDescription: string;
    projectCopy: string;
    projectImgPrincipal: Img;
    myRolTitle: string;
    myRolDescription: string;
    viewTitle: string;
    viewImg: Img;
    viewCopy: string;
    view2Title: string;
    view2Img: Img;
    view2Copy: string;
}

export interface Img {
    url: string;
    alt: string;
}
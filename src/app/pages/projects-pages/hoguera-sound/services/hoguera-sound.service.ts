import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { ENDPOINTS } from 'src/app/endpoints/endPoints';
import { Ihoguera } from '../models/IHoguera';

@Injectable({
  providedIn: 'root'
})
export class HogueraSoundService {
private hogueraEndPoint: string = ENDPOINTS.hogueraSound;

  constructor(private http: HttpClient) {/*Empty*/ }

  public getHoguera(): Observable<any> {
    return this.http.get(this.hogueraEndPoint).pipe(
      map((response: Ihoguera) => {
        if (!response) {
          throw new Error ('value expected')
        } else {
          return response;
        }
      }),
      catchError ((err)=> {
        throw new Error (err.message);
      })
    );
  }
}

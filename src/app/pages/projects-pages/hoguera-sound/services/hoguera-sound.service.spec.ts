import { TestBed } from '@angular/core/testing';

import { HogueraSoundService } from './hoguera-sound.service';

describe('HogueraSoundService', () => {
  let service: HogueraSoundService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HogueraSoundService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

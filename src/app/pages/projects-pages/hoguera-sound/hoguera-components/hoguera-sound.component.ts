import { HogueraSoundService } from './../services/hoguera-sound.service';
import { Ihoguera } from './../models/IHoguera';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hoguera-sound',
  templateUrl: './hoguera-sound.component.html',
  styleUrls: ['./hoguera-sound.component.scss']
})
export class HogueraSoundComponent implements OnInit {
  public dataHoguera: Ihoguera;

  constructor(private hogueraSoundService: HogueraSoundService) { }

  ngOnInit(): void {
    this.getHogueraData();
  }

  public getHogueraData():void {
    this.hogueraSoundService.getHoguera().subscribe(
      (data: Ihoguera) => {
        this.dataHoguera = data;
        console.log(this.dataHoguera);
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

}
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HogueraSoundComponent } from './hoguera-sound.component';

describe('HogueraSoundComponent', () => {
  let component: HogueraSoundComponent;
  let fixture: ComponentFixture<HogueraSoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HogueraSoundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HogueraSoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HogueraSoundComponent } from './hoguera-components/hoguera-sound.component';

const routes: Routes = [
  {
    path:'',
    component: HogueraSoundComponent,

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HogueraSoundRoutingModule { }

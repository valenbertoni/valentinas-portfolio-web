export const ENDPOINTS  = {
    work: 'http://localhost:3000/getWork',
    about:  'http://localhost:3000/getAbout',
    contact: 'http://localhost:3000/getContact',
    dataUser: 'http://localhost:3000/dataUser',
    artGallery: 'http://localhost:3000/artGallery',
    hogueraSound: 'http://localhost:3000/hogueraSound',
    sounditi: 'http://localhost:3000/sounditi'

};
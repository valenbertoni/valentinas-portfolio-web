import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', redirectTo: 'work', pathMatch: 'full',
  },
  {
    path: 'work',
    loadChildren: () =>
      import('./pages/work/work.module').then((m) => m.WorkModule),
  },
  {
    path: 'about',
    loadChildren: () =>
      import('./pages/about/about.module').then((m) => m.AboutModule),
  },
  {
    path: 'contact',
    loadChildren: () =>
      import('./pages/contact/contact.module').then((m) => m.ContactModule),
  },
  {
    path: 'artGallery',
    loadChildren: () =>
      import('./pages/projects-pages/art-gallery/art-gallery.module').then((m) => m.ArtGalleryModule),
  },
  {
    path: 'hogueraSound',
    loadChildren: () =>
      import('./pages/projects-pages/hoguera-sound/hoguera-sound.module').then((m) => m.HogueraSoundModule),
  },
  {
    path: 'sounditi',
    loadChildren: () =>
      import('./pages/projects-pages/sounditi/sounditi.module').then((m) => m.SounditiModule),
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
